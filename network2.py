# simple python script to train a 1-layer neural network to classify cifar10 images use the tensorflow library

import numpy as np
import tensorflow as tf
import os
# class written to replicate input_data from tensorflow.examples.tutorials.mnist for CIFAR-10
import cifar10_read

# training parameters
nbatch = 200
learning_rate = .001
n_iter = 10000

# location of the CIFAR-10 dataset
#CHANGE THIS PATH TO THE LOCATION OF THE CIFAR-10 dataset on your local machine
data_dir = "./Datasets/cifar-10-batches-py/"

# read in the dataset
print('reading in the CIFAR10 dataset')
dataset = cifar10_read.read_data_sets(data_dir, one_hot=True, reshape=False, distort_train=True)   

using_tensorboard = True

##################################################
# PHASE 1  - ASSEMBLE THE GRAPH

# 1.1) define the placeholders for the input data and the ground truth labels

# x_input can handle an arbitrary number of input vectors of length input_dim = d 
# y_  are the labels (each label is a length 10 one-hot encoding) of the inputs in x_input
# If x_input has shape [N, input_dim] then y_ will have shape [N, 10]

def batch_norm(x, is_training):
    shape = x.get_shape().as_list()
    
    gamma = tf.Variable(tf.ones(shape[-1]))
    beta = tf.Variable(tf.zeros(shape[-1]))

    pop_mean = tf.Variable(tf.zeros(shape[-1]), trainable=False)
    pop_var = tf.Variable(tf.ones(shape[-1]), trainable=False)

    batch_mean, batch_var = tf.nn.moments(x, range(len(shape)-1))

    decay = .9 * is_training[0] + 1 * (1 - is_training[0])
    train_mean = tf.assign(pop_mean, pop_mean * decay + batch_mean * (1 - decay))
    train_var = tf.assign(pop_var, pop_var * decay + batch_var * (1 - decay))

    with tf.control_dependencies([train_mean, train_var]):

        u_mean = batch_mean * is_training[0] + pop_mean * (1 - is_training[0])
        u_var = batch_var * is_training[0] + pop_var * (1 - is_training[0])

        return tf.nn.batch_normalization(x, u_mean, u_var, beta, gamma, 10e-8)

input_dim = 32*32*3    # d
x_input = tf.placeholder(tf.float32, shape = [None, 32, 32, 3], name='x_input')
y_ = tf.placeholder(tf.float32, shape = [None, 10], name='y_')
is_training = tf.placeholder(tf.float32, shape=[1], name='is_training')
x_input.get_shape()
tf.Print(x_input, [x_input], message='x_input: ')

x_input_norm = batch_norm(x_input, is_training)

conv = tf.layers.conv2d(
    inputs=x_input_norm,
    filters=32,
    kernel_size=[5,5],
    padding="same")
conv = batch_norm(conv, is_training)
conv = tf.nn.relu(conv)

pool = tf.layers.max_pooling2d(inputs=conv, pool_size=[2, 2], strides=2)

conv = tf.layers.conv2d(
    inputs=pool,
    filters=64,
    kernel_size=[3,3],
    padding="same")
conv = batch_norm(conv, is_training)
conv = tf.nn.relu(conv)

pool = tf.layers.max_pooling2d(inputs=conv, pool_size=[2, 2], strides=2)

conv = tf.layers.conv2d(
    inputs=pool,
    filters=64,
    kernel_size=[3,3],
    padding="same")
conv = batch_norm(conv, is_training)
conv = tf.nn.relu(conv)

pool = tf.layers.max_pooling2d(inputs=conv, pool_size=[2, 2], strides=2)

conv = tf.layers.conv2d(
    inputs=pool,
    filters=64,
    kernel_size=[3,3],
    padding="same")
conv = batch_norm(conv, is_training)
conv = tf.nn.relu(conv)

pool = tf.layers.max_pooling2d(inputs=conv, pool_size=[2, 2], strides=2)

flat = tf.reshape(pool, [-1, 2 * 2 * 64])

dense = tf.layers.dense(
    inputs=flat,
    units=64,
    activation=tf.nn.relu,
    kernel_initializer=tf.contrib.layers.xavier_initializer()
)
dense = batch_norm(dense, is_training)
y = tf.layers.dense(inputs = dense, units = 10, kernel_initializer=tf.contrib.layers.xavier_initializer())

# 1.4) define the loss funtion 
# cross entropy loss: 
# Apply softmax to each output vector in y to give probabilities for each class then compare to the ground truth labels via the cross-entropy loss and then compute the average loss over all the input examples
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))

# 1.5) Define the optimizer used when training the network ie gradient descent or some variation.
# Use gradient descent with a learning rate of .01
train_step = tf.train.AdamOptimizer().minimize(cross_entropy)

# (optional) definiton of performance measures
# definition of accuracy, count the number of correct predictions where the predictions are made by choosing the class with highest score
correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))  

# 1.6) Add an op to initialize the variables.
init = tf.global_variables_initializer()

##################################################

def evaluate(tensors, sess, data, nbatch, labels):
    vals = []
    data_chunks = [ data[i:i+nbatch,:,:,:] for i in xrange(0, np.shape(data)[0], nbatch) ]
    label_chunks = [ labels[i:i+nbatch,:] for i in xrange(0, np.shape(labels)[0], nbatch) ]
    for i in range(np.shape(data_chunks)[0]):
        data_chunk = data_chunks[i]
        label_chunk = label_chunks[i]
        vals.append(sess.run(tensors, feed_dict={x_input:data_chunk, y_: label_chunk, is_training: [0]}))
    vals = np.array(vals)
    return vals.mean(axis=0).tolist()

##################################################


# If using TENSORBOARD
if using_tensorboard:
    # keep track of the loss and accuracy for the training set
    tf.summary.scalar('training loss', cross_entropy, collections=['training'])
    tf.summary.scalar('training accuracy', accuracy, collections=['training'])
    # merge the two quantities
    tsummary = tf.summary.merge_all('training')
    
    # keep track of the loss and accuracy for the validation set
    tf.summary.scalar('validation loss', cross_entropy, collections=['validation'])
    tf.summary.scalar('validation accuracy', accuracy, collections=['validation'])
    # merge the two quantities
    vsummary = tf.summary.merge_all('validation')

##################################################


##################################################
# PHASE 2  - PERFORM COMPUTATIONS ON THE GRAPH

# 2.1) start a tensorflow session
with tf.Session() as sess:

    ##################################################
    # If using TENSORBOARD
    if using_tensorboard:
        # set up a file writer and directory to where it should write info + 
        # attach the assembled graph
        summary_writer = tf.summary.FileWriter('network2/results', sess.graph)
    ##################################################

    # 2.2)  Initialize the network's parameter variables
    # Run the "init" op (do this when training from a random initialization)
    sess.run(init) 

    # 2.3) loop for the mini-batch training of the network's parameters
    for i in range(n_iter):
        
        # grab a random batch (size nbatch) of labelled training examples
        batch = dataset.train.next_batch(nbatch)

        # create a dictionary with the batch data 
        # batch data will be fed to the placeholders for inputs "x_input" and labels "y_"

        batch_dict = {
            x_input: batch[0], # input data
            y_: batch[1], # corresponding labels
            is_training: [1]
        }
        
        # run an update step of mini-batch by calling the "train_step" op 
        # with the mini-batch data. The network's parameters will be updated after applying this operation
        sess.run(train_step, feed_dict=batch_dict)

        # periodically evaluate how well training is going
        if i % 50 == 0:

            # compute the performance measures on the training set by
            # calling the "cross_entropy" loss and "accuracy" ops with the training data fed to the placeholders "x_input" and "y_"
            
            #tr = sess.run([cross_entropy, accuracy], feed_dict = {x_input:dataset.train.images, y_: dataset.train.labels})
            tr = evaluate([cross_entropy, accuracy], sess, dataset.train.images, 5000, dataset.train.labels)

            # compute the performance measures on the validation set by
            # calling the "cross_entropy" loss and "accuracy" ops with the validation data fed to the placeholders "x_input" and "y_"

            val = sess.run([cross_entropy, accuracy], feed_dict={x_input:dataset.validation.images, y_:dataset.validation.labels, is_training: [0]})

            info = [i] + tr + val
            print(info)

            ##################################################
            # If using TENSORBOARD
            if using_tensorboard:

                # compute the summary statistics and write to file
                summary_str = sess.run(tsummary, feed_dict = {x_input:dataset.train.images[1:5000], y_: dataset.train.labels[1:5000], is_training: [0]})
                summary_writer.add_summary(summary_str, i)

                summary_str1 = sess.run(vsummary, feed_dict = {x_input:dataset.validation.images, y_: dataset.validation.labels, is_training: [0]})
                summary_writer.add_summary(summary_str1, i)
            ##################################################

    # evaluate the accuracy of the final model on the test data
    #test_acc = sess.run(accuracy, feed_dict={x_input: dataset.test.images, y_: dataset.test.labels})
    test_acc = evaluate([accuracy], sess, dataset.test.images, nbatch, dataset.test.labels)
    final_msg = 'test accuracy:' + str(test_acc)
    print(final_msg)

##################################################
